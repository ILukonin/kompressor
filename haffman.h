#ifndef HAFFMAN_H
#define HAFFMAN_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



int compressor(char * source_name, char * dest_name); //компрессор
int decompressor(char * source_name, char * dest_name); //декомпрессор


#endif // HAFFMAN_H
