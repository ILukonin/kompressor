#include <stdio.h>

#include "haffman.h"

enum code_meny {WRONG_KEY, COMPRESS, DECOMPRESS};



void menu_lst()
{

    printf("Compress and decompress file. Create arch DEST.hfm\n");
    printf("Usage:\n");
    printf("Kompressor -[cdh] [SOURCE] [DEST]:\n");
    printf("\t-c:  compress file SOURCE to [DEST]\n");
    printf("\t-d:  decompress file SOURCE to [DEST]\n");
    printf("\t-h:  this message\n");
}

int command_line(char * str)
{

    if( (strlen(str) < 2)|| ( str[0] != '-' ) )
    {
        printf("Unknown key %s\n",str);
        return 0;
    } else
    {
        switch (str[1])
        {
        case 'h': return WRONG_KEY;
                  break;
        case 'c': return COMPRESS;
                  break;
        case 'd': return DECOMPRESS;
                  break;
        default:
                printf("Unknown key %s\n",str);
                return WRONG_KEY;
                break;
        }
    }



}


int main(int argc,char * argv[])
{


    if(argc < 3)
    {
        printf("Too few arguments!\n");
        menu_lst();
        return 1;
    }

    switch ( command_line(argv[1]) )
    {
    case WRONG_KEY: menu_lst();
         break;
    case COMPRESS:
        if(argc == 3)
            compressor(argv[2], NULL);
        else
            compressor(argv[2], argv[3]);
        break;
    case DECOMPRESS:
        if(argc == 3)
            decompressor(argv[2], NULL);
        else
            decompressor(argv[2], argv[3]);
        break;
    default:
        break;
    }


    return 0;
}


