#include "haffman.h"

#define BYTE_COUNT  256

unsigned char byte_mask[8]={128,64,32,16,8,4,2,1}; //набор битовых масок
const char * VERSION_AND_LABEL_COMPRESSOR = "КрымНаш!"; //метка для определения архив ли это или же нет.


struct byte_STRUCT  // основная структура для хранения одного байта
{
    unsigned char ch;        //тут хранится один байт
    float freq;     //тут будет частота встречаемости.
    char * code;      //код хаффмана

    struct byte_STRUCT * left;
    struct byte_STRUCT * right;    //указатели на левую и правую ветвь соответственно

};

typedef struct byte_STRUCT BYTE_STRUCT;


struct byte_STRUCT_file     //структура для записи в файл
{
    unsigned char ch;
    float freq;

};

typedef struct byte_STRUCT_file BYTE_STRUCT_FILE;

static void copy_array_ptr(BYTE_STRUCT ** dest_array,BYTE_STRUCT ** source_array,const int count_ptr) // физически копируем массив в массив
{
    int i = 0;
    for(i=0;i<count_ptr;i++)
        dest_array[i]=source_array[i];
}

static void print_byte(const BYTE_STRUCT byte)    //выводим на экран один байт с его свойствами. для отладки
{

    printf("char %c(%d)\nfreq %f\ncode %s\n----------------\n",byte.ch,byte.ch,byte.freq,byte.code);
}

static void ** sort_ptr_array(BYTE_STRUCT ** ptr_array,const int count_ptr) //да тупо пузырьком отсортируем. тут нет смысла заморачиваться, всего 256 элементов
{

    BYTE_STRUCT * tmp = NULL;
    int i = 0;
    int flag = 0;
    int count = 0;


    while(flag == 0)
    {
        flag++;
        for(i=0;i<count_ptr-2;i++)

            if(ptr_array[i]->freq < ptr_array[i+1]->freq) //сортируем по убыванию для удобства
            {
                flag = 0;
                tmp = ptr_array[i];
                ptr_array[i]=ptr_array[i+1];
                ptr_array[i+1]=tmp;


                count++;
            }

    }

    return NULL;
}

static void initialyze_array(BYTE_STRUCT char_array[])  // инициализация массива байт
{
    int i = 0;

    for(i=0;i< BYTE_COUNT; i++)
    {
        char_array[i].ch = i; //инициализация массива
        char_array[i].freq = 0;
        char_array[i].code = NULL;
        char_array[i].left = NULL;
        char_array[i].right = NULL;
    }
}

static BYTE_STRUCT ** analyze_file(BYTE_STRUCT char_array[], FILE * file, int * count_ptr)      //анализ файла. формируем в массиве байт частоту встречаемости каждого байта
{
    BYTE_STRUCT ** ptr_array = NULL;
    BYTE_STRUCT ** tmp_ptr_array = NULL;
    int buf = 0;
    int count = 0;
    int i = 0;
    int len = 0;

    ptr_array = (BYTE_STRUCT **)calloc(1,sizeof(BYTE_STRUCT * )); // отдаем массив указателей на ненулевые байты
    if( ptr_array == NULL)
    {
        printf("Can't allocate mem for ptr_array in analyze_file()\n");
        return NULL;
    }




    while( (buf = getc(file)) != EOF)
    {
        char_array[buf].freq++;  //количество каждого символа
        count++;            //количество прочитанных символов
    }




    for(i=0;i< BYTE_COUNT; i++)
    {

        if(char_array[i].freq > 0.)
        {

            char_array[i].freq /= (float)count; //нормировка

            ptr_array[len] = &(char_array[i]);
            len++;

            tmp_ptr_array = (BYTE_STRUCT **)realloc(ptr_array,sizeof(BYTE_STRUCT * ) * (len+1));
            if( tmp_ptr_array == NULL)
            {
                printf("Can't reallocate mem for tmp_ptr_array in analyze_file()\n");
                return NULL;
            }

            ptr_array = tmp_ptr_array;


        }

    }

    *count_ptr = len;
    sort_ptr_array(ptr_array,len);

    return ptr_array;

}





static int insert_element(BYTE_STRUCT ** ptr_array, const int count_ptr, BYTE_STRUCT * source_ptr) // в массив указателей вставляем новый указатель с сохранением убывания по частотам
{
    BYTE_STRUCT ** tmp_array = (BYTE_STRUCT **)calloc(count_ptr,sizeof(BYTE_STRUCT*));
    int flag = 0;
    int i = 0;

    if(tmp_array == NULL)
    {
        printf("can't allocate mem for tmp_array int insert_element()!\n");
        return 1;
    }



    copy_array_ptr(tmp_array,ptr_array,count_ptr); // заодно сделаем так, чтоб массив лежал в одном месте

    for(i=0;i<count_ptr;i++)
    {
        if((source_ptr->freq > tmp_array[i]->freq)&&(flag == 0))
        {
            ptr_array[i] = source_ptr;
            flag = 1;

        } else
            ptr_array[i] = tmp_array[i-flag];
    }


    return 0;
}



static BYTE_STRUCT * build_haffman_tree(BYTE_STRUCT ** ptr_array,int count_ptr) //вернёт указатель на корень дерева
{
    BYTE_STRUCT * temp_root = (BYTE_STRUCT *)calloc(1,sizeof(BYTE_STRUCT)); //новый узел дерева

    if(count_ptr < 2)
        return ptr_array[0]; //пока так.

    temp_root->freq = ptr_array[count_ptr-2]->freq + ptr_array[count_ptr-1]->freq; //пусть указывает на два предыдущих узла
    temp_root->code = NULL;

    temp_root->left = ptr_array[count_ptr-2];   // в лево пойдём
    temp_root->right = ptr_array[count_ptr-1];


    if(count_ptr <= 2)
        return temp_root;


    insert_element(ptr_array,count_ptr,temp_root);
    count_ptr--;


    return build_haffman_tree(ptr_array,count_ptr);

}




static void coding_tree(BYTE_STRUCT * root, char * code)        //проходимся по всему дереву и формируем коды
{
    char * tmp = (char *)calloc(strlen(code)+2,sizeof(char));

    root->code = (char *)calloc(strlen(code)+2,sizeof(char));

    strcpy(tmp,code);
    strcpy(root->code,code);

    tmp[strlen(code)] = '1';
    if(root->left != NULL)
        coding_tree(root->left,tmp);

    tmp[strlen(code)] = '0';
    if(root->right != NULL)
        coding_tree(root->right,tmp);
    free(tmp);

}



static void compress_text(FILE * source, FILE * dest, BYTE_STRUCT * byte_array) // для отладки. печатаем не побитно, а посимвольно.
{
    int tmp = 0;
    while( (tmp=getc(source)) != EOF)
        fprintf(dest,"%s",byte_array[tmp].code);
}


static int compress(FILE * source, FILE * dest, BYTE_STRUCT * byte_array)  // собственно, упаковка файла.
{

    unsigned char temp = 0;
    unsigned int buf = 0;
    unsigned int i = 0;
    int j = 0;

    while( (buf = fgetc(source)) != EOF )
    {

        for(i=0;i<strlen(byte_array[buf].code);i++)
        {

            if( (byte_array[buf].code)[i] == '1') //ставим нужный бит.
            {
                temp = temp | byte_mask[j];

            }

            j++;

            if(j == 8)
            {
                fputc(temp,dest);
                j = 0;
                temp = 0;
            }

        }
    }




    if(j != 0) //если место ещё не кончилось, допечатываем нули в конце. надо придумать, как последний байт потом считывать
    {
        fputc(temp,dest);//записали последний байт

    }

    return j; //вернули размер хвоста

}

static void decompress(FILE * source, FILE * dest, BYTE_STRUCT * root_ptr)
{
    int buf = 0;
    int next_buf = 0;
    unsigned int size_byte = 0; // будем хранить количество значимых символов в байте
    unsigned int i = 0;
    BYTE_STRUCT * temp_ptr = root_ptr;
    int tail = 0; //хвост

    fread(&tail,sizeof(int),1,source);//считали хвост

    buf = fgetc(source);

    while( buf  != EOF )
    {
        next_buf = fgetc(source); //заглянем в будущее =)
        if( next_buf != EOF)
            size_byte = 8;
        else
            size_byte = tail > 0? tail:8; //если хвост есть, то печатаем в размер хвоста, иначе печатаем весь байт

        for(i=0;i < size_byte; i++)
        {
            if( (temp_ptr->left == NULL) && (temp_ptr->right == NULL)) //считаем, что коды у нас записанны корректно и мы всегда попадаем в лист
            {
                fputc(temp_ptr->ch,dest);
                temp_ptr = root_ptr;

            }

            if( (buf & byte_mask[i]) == byte_mask[i] )
                temp_ptr = temp_ptr->left;
            else
                temp_ptr = temp_ptr->right;


        }
        buf = next_buf;

    }

    if( (temp_ptr->left == NULL) && (temp_ptr->right == NULL)) //допечатаем последний символ
    {
        fputc(temp_ptr->ch,dest);
        temp_ptr = root_ptr;

    }


return;
}


static void print_header(FILE * dest, BYTE_STRUCT * byte_array,const int count) // печатаем заголовок
{
    int i = 0;
    BYTE_STRUCT_FILE temp_struct;


    fprintf(dest,"%s",VERSION_AND_LABEL_COMPRESSOR);
    fprintf(dest,"%d",count);
    for(i = 0;i < BYTE_COUNT; i++)
    {
        if(byte_array[i].freq > 0.)
        {
            temp_struct.ch = byte_array[i].ch;
            temp_struct.freq = byte_array[i].freq;
            fwrite(&temp_struct,sizeof(BYTE_STRUCT_FILE),1,dest);

        }
    }

    fwrite(&count,sizeof(int),1,dest);//запишем временный инт для хранения хвоста

}


static int read_header(FILE * source, BYTE_STRUCT * byte_array) //читаем заголовок
{
    int i = 0;
    int count = 0;
    BYTE_STRUCT_FILE temp_struct;
    char * tmp_char = (char *)calloc(strlen(VERSION_AND_LABEL_COMPRESSOR),sizeof(char)+1);

    fread(tmp_char,sizeof(char),strlen(VERSION_AND_LABEL_COMPRESSOR),source);
    if(strcmp(tmp_char,VERSION_AND_LABEL_COMPRESSOR) != 0)
       return 0;


    fscanf(source,"%d",&count);

    for(i = 0;i < count; i++)
    {

            fread(&temp_struct,sizeof(BYTE_STRUCT_FILE),1,source);
            byte_array[temp_struct.ch].freq = temp_struct.freq;


    }


    return count;

}


int compressor(char * source_name, char * dest_name) //здесь будет производиться запаковка файла
{
    FILE * source_file = NULL;
    FILE * dest_file = NULL;
    BYTE_STRUCT byte_array[BYTE_COUNT];
    BYTE_STRUCT * root_ptr;
    BYTE_STRUCT ** byte_ptr_array = NULL;
    int len_ptr = 0;
    int tail = 0; //будем хранить длину хвоста


    printf("Open file %s\n",source_name);
    source_file = fopen(source_name,"rb");
    if( source_file == NULL)
    {
        perror("Can't open file!\n");
        return 2;
    }

    printf("OK\nPrepearing...\n");

    initialyze_array(byte_array);     //инициализировали массив
    byte_ptr_array = analyze_file(byte_array,source_file, &len_ptr);   //создали массив указателей и заполнили таблицу встречаемости

    fclose(source_file);


    if(byte_ptr_array == NULL)
    {
        printf("Error!\n");
        return 3;
    }

    root_ptr = build_haffman_tree(byte_ptr_array,len_ptr); //построили дерево Хаффмана
    if(root_ptr == NULL)
    {
        printf("Error!\n");
        return 4;
    }

    coding_tree(root_ptr,""); //Закодировали путь до листьев



    source_file = fopen(source_name,"rb");
    if( source_file == NULL)
    {
        perror("Can't open file!\n");
        return 2;
    }

    if(dest_name == NULL)
    {
        dest_name = (char *)calloc(strlen(source_name) + 4,sizeof(char));
        strcpy(dest_name,source_name);
        strcat(dest_name,".hfm");   // сформировали имя для выходного файла
    }

    printf("Open destination file %s\n",dest_name);

    dest_file = fopen(dest_name,"wb");
    if( dest_file == NULL)
    {
        perror("Can't open file!\n");
        return 2;
    }

    printf("OK\nCompressing...\n");
    print_header(dest_file,byte_array, len_ptr); // напечатали заголовок
    tail = compress(source_file,dest_file,byte_array); // запаковали

    fclose(source_file);
    fclose(dest_file);

    dest_file = fopen(dest_name,"r+"); //еще раз открываем архив, чтоб прочитать заголовок и вписать размер хвоста
    read_header(dest_file,byte_array);
    //fprintf(dest_file,"%d",tail);//запишем хвост
    fwrite(&tail,sizeof(int),1,dest_file);
    fclose(dest_file);


    printf("Compressing succesfull!\n");

    return 0;
}



int decompressor(char * source_name, char * dest_name)      // тут будем распаковывать файл
{
    FILE * source_file = NULL;
    FILE * dest_file = NULL;
    BYTE_STRUCT byte_array[BYTE_COUNT];
    BYTE_STRUCT * root_ptr;
    BYTE_STRUCT ** byte_ptr_array = NULL;
    int i = 0;
    int j = 0;
    int len_ptr = 0;



    printf("Open file %s\n",source_name);

    source_file = fopen(source_name,"rb");
    if( source_file == NULL)
    {
        perror("Can't open file!\n");
        return 2;
    }

    printf("OK\nPreparing...\n");

    initialyze_array(byte_array); // инициализировали массив

    len_ptr = read_header(source_file,byte_array); // прочитали из заголовка частоты и количество ненулевых элементов

    if(len_ptr == 0)
    {
        printf("Missing file %s!\n",source_name);
        return 1;
    }

    byte_ptr_array = (BYTE_STRUCT **)calloc(len_ptr,sizeof(BYTE_STRUCT*));//создаём массив указателей
    if( byte_ptr_array == NULL)
    {
        printf("Can't allocate mem for ptr_array in decompressor()\n");
        return 1;
    }


    for(i = 0; i < BYTE_COUNT; i++)     //связываем указатели с ненулевыми элементами в массиве
    {
        if(byte_array[i].freq > 0.)
        {
            byte_ptr_array[j] = &(byte_array[i]);
            j++;
        }
    }

    sort_ptr_array(byte_ptr_array,len_ptr); // отсортировали массив

    root_ptr = build_haffman_tree(byte_ptr_array,len_ptr); //построили дерево Хаффмана
    if(root_ptr == NULL)
    {
        printf("Error!\n");
        return 4;
    }


    coding_tree(root_ptr,""); //Закодировали путь до листьев снова

    if(dest_name == NULL)
    {
        dest_name = (char *)calloc(strlen(source_name) + 5,sizeof(char));
        strcpy(dest_name,source_name);
        strcat(dest_name,".orig");   // сформировали имя для выходного файла
    }

    printf("Open destination file %s\n",dest_name);

    dest_file = fopen(dest_name,"wb");
    if( dest_file == NULL)
    {
        perror("Can't open file!\n");
        return 2;
    }


    printf("OK\nDecompressing...\n");
    decompress(source_file,dest_file,root_ptr);

    fclose(source_file);
    fclose(dest_file);

    printf("Decompressing succesfull!\n");

    return 0;
}
